# README #

This repository is simply a collection of algorithms (in Java) for discerning prime numbers.

These algorithms are maximized for the size or number of primes determined, and secondarily the speed at which they are found.