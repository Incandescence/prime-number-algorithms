package com.gildation.prime;

public class PrimeSieveSimpleWithBytes implements PrimeSieve
{

	private final byte[] sieve;
	private final int actualSieveSize;

	public PrimeSieveSimpleWithBytes(final long numberOfPrimes)
	{
		sieve = new byte[(int) numberOfPrimes];
		actualSieveSize = sieve.length;
		final int requiredComputeRange = (int) Math.sqrt(actualSieveSize) + 1;

		sieve[0] = 1;
		sieve[1] = 1;

		unsetMultiples(2);
		unsetMultiples(3);

		for (int i = 5; i < requiredComputeRange; i += 2)
		{
			if (sieve[i] == 0)
			{
				unsetMultiples(i);
			}
		}
	}

	private void unsetMultiples(final int x)
	{
		int i = x * x;
		do
		{
			sieve[i] = 1;
			i += x;
		} while (i < actualSieveSize);
	}

	@Override
	public boolean isPrime(final long i)
	{
		return sieve[(int) i] == 0;
	}

	@Override
	public long findLargestPrimeFound()
	{
		int i = actualSieveSize - 1;

		while (sieve[i] != 0)
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return actualSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		long count = 0;

		for (int i = 0; i < actualSieveSize; i++)
		{
			if (sieve[i] == 0)
			{
				count++;
			}
		}

		return count;
	}

	@Override
	public long size()
	{
		return actualSieveSize;
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

}
