package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveIncrementTwice implements PrimeSieve
{

	private final BitArray sieve;
	private final long actualSieveSize;

	public PrimeSieveIncrementTwice(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes);
		actualSieveSize = sieve.size();
		final long requiredComputeRange = (long) Math.sqrt(actualSieveSize) + 1;

		sieve.unset(0);
		sieve.unset(1);

		for (long i = 4; i < actualSieveSize; i += 2)
		{
			sieve.unset(i);
		}

		for (long i = 9; i < actualSieveSize; i += 6)
		{
			sieve.unset(i);
		}

		long incrementor = 5;

		do
		{
			if (sieve.isSet(incrementor))
			{
				unsetMultiples(incrementor);
			}

			incrementor += 2;

			if (sieve.isSet(incrementor))
			{
				unsetMultiples(incrementor);
			}

			incrementor += 4;
		} while (incrementor < requiredComputeRange);

		// if (sieve.isSet(incrementor))
		// {
		// unsetMultiples(incrementor);
		// }
	}

	private void unsetMultiples(final long x)
	{
		for (long i = x * x, iIncr = x * 2; i < actualSieveSize; i += iIncr)
		{
			sieve.unset(i);
		}
	}

	@Override
	public boolean isPrime(final long i)
	{
		return sieve.isSet(i);
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = actualSieveSize - 1;

		while (!sieve.isSet(i))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return actualSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits();
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

}
