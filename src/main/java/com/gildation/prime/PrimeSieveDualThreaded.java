package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveDualThreaded implements PrimeSieve
{

	private final BitArray sieve;
	private final long actualSieveSize;

	public PrimeSieveDualThreaded(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes);
		actualSieveSize = sieve.size();
		final long requiredComputeRange = (long) Math.sqrt(actualSieveSize) + 1;

		sieve.unset(0);
		sieve.unset(1);

		unsetMultiples(2);
		unsetMultiples(3);

		final long sixes = numberOfPrimes / 6;
		final long start = (long) (0.0001 * sixes);

		for (long i = 6, iMax = start * 6; i < iMax; i += 6)
		{
			if (sieve.isSet(i - 1))
			{
				unsetMultiples(i - 1);
			}

			if (sieve.isSet(i + 1))
			{
				unsetMultiples(i + 1);
			}
		}

		new Thread()
		{
			@Override
			public void run()
			{
				for (long i = start * 6 - 1; i < requiredComputeRange; i += 6)
				{
					if (sieve.isSet(i))
					{
						unsetMultiples(i);
					}
				}
			}
		}.start();

		for (long i = start * 6 + 1; i < requiredComputeRange; i += 6)
		{
			if (sieve.isSet(i))
			{
				unsetMultiples(i);
			}
		}
	}

	private void unsetMultiples(final long x)
	{
		for (long i = x * x; i < actualSieveSize; i += x)
		{
			sieve.unset(i);
		}
	}

	@Override
	public boolean isPrime(final long i)
	{
		return sieve.isSet(i);
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = actualSieveSize - 1;

		while (!sieve.isSet(i))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return actualSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits();
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

}
