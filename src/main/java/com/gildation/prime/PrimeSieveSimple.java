package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveSimple implements PrimeSieve
{

	private final BitArray sieve;
	private final long actualSieveSize;

	public PrimeSieveSimple(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes);
		actualSieveSize = sieve.size();
		final long requiredComputeRange = (long) Math.sqrt(actualSieveSize) + 1;

		sieve.unset(0);
		sieve.unset(1);

		unsetMultiples(2);
		unsetMultiples(3);

		for (long i = 6; i < requiredComputeRange; i += 6)
		{
			if (sieve.isSet(i - 1))
			{
				unsetMultiples(i - 1);
			}

			if (sieve.isSet(i + 1))
			{
				unsetMultiples(i + 1);
			}
		}
	}

	private void unsetMultiples(final long x)
	{
		long i = x * x;
		do
		{
			sieve.unset(i);
			i += x;
		} while (i < actualSieveSize);
	}

	@Override
	public boolean isPrime(final long i)
	{
		return sieve.isSet(i);
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = actualSieveSize - 1;

		while (!sieve.isSet(i))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return actualSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits();
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

}
