package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveDoubleIncrementers implements PrimeSieve
{

	private final BitArray sieve;
	private final long actualSieveSize;

	public PrimeSieveDoubleIncrementers(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes);
		actualSieveSize = sieve.size();
		final long requiredComputeRange = (long) Math.sqrt(actualSieveSize) + 1;

		sieve.unset(0);
		sieve.unset(1);

		unsetMultiples(2);
		unsetMultiples(3);

		long oneLessOffset = 5;
		long oneMoreOffset = 7;

		do
		{
			if (sieve.isSet(oneLessOffset))
			{
				unsetMultiples(oneLessOffset);
			}

			if (sieve.isSet(oneMoreOffset))
			{
				unsetMultiples(oneMoreOffset);
			}

			oneLessOffset += 6;
			oneMoreOffset += 6;
		} while (oneMoreOffset < requiredComputeRange);

		if (sieve.isSet(oneLessOffset))
		{
			unsetMultiples(oneLessOffset);
		}
	}

	private void unsetMultiples(final long x)
	{
		for (long i = x * x; i < actualSieveSize; i += x)
		{
			sieve.unset(i);
		}
	}

	@Override
	public boolean isPrime(final long i)
	{
		return sieve.isSet(i);
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = actualSieveSize - 1;

		while (!sieve.isSet(i))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return actualSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits();
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

}
