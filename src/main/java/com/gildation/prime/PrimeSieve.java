package com.gildation.prime;

public interface PrimeSieve
{

	public boolean isPrime(final long i);

	public long findLargestPrimeFound();

	public long effectiveSieveSize();

	public long countNumberOfPrimesFound();

	public long size();

}
