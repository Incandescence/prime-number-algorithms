package com.gildation.prime.models;

import java.util.PriorityQueue;

public class FactorPrimePair implements Comparable<FactorPrimePair>
{
	private Long factor;
	private final Long prime;

	public FactorPrimePair(final Long factor, final Long prime)
	{
		this.factor = factor;
		this.prime = prime;
	}

	public Long getFactor()
	{
		return factor;
	}

	public void incrementFactor()
	{
		factor += prime;
	}

	public Long getPrime()
	{
		return prime;
	}

	@Override
	public int compareTo(final FactorPrimePair that)
	{
		return (this.factor - that.factor) < 0 ? -1 : 1;
	}

	@Override
	public String toString()
	{
		return "[key=" + factor + ", value=" + prime + "]";
	}

	public static void main(final String[] args)
	{
		final PriorityQueue<FactorPrimePair> queue = new PriorityQueue<FactorPrimePair>();
		queue.add(new FactorPrimePair(0l, 321l));
		queue.add(new FactorPrimePair(1l, 521l));
		queue.add(new FactorPrimePair(2l, 121l));

		System.out.println(queue.poll());
		System.out.println(queue.poll());
		System.out.println(queue.poll());
		System.out.println(queue.poll());
	}
}
