package com.gildation.prime.models;

public class BitArray
{
	private final static int[] SELECT_INDEX;
	private final static int[] INVERSE_SELECT_INDEX;

	static
	{ // Pre-compute indexes
		final int indexSize = 32;

		SELECT_INDEX = new int[indexSize];
		INVERSE_SELECT_INDEX = new int[indexSize];

		for (int i = 0; i < indexSize; i++)
		{
			SELECT_INDEX[i] = 1 << i;
			INVERSE_SELECT_INDEX[i] = ~(1 << i);
		}
	}

	private final int[] partitions;

	public BitArray(final long size)
	{
		final int numberOfPartitions = (int) (size / 32) + 1;

		if (numberOfPartitions > Integer.MAX_VALUE)
		{
			throw new RuntimeException(new StringBuilder("Can not create a BitArray with ").append(size)
					.append(" indexes. Largest possible BitArray size is ").append(Integer.MAX_VALUE * 32l).append(".")
					.toString());
		}

		if (0 >= numberOfPartitions)
		{
			throw new RuntimeException(new StringBuilder("Can not create a BitArray with ").append(size)
					.append(" indexes. Minimum possible BitArray size is 32.").toString());
		}

		partitions = new int[numberOfPartitions];
	}

	public boolean isSet(final long i)
	{
		return (partitions[(int) (i / 32)] & SELECT_INDEX[(int) (i % 32)]) == 0;
	}

	public void unset(final long i)
	{
		final int partitionId = (int) (i / 32);
		partitions[partitionId] = partitions[partitionId] | SELECT_INDEX[(int) (i % 32)];
	}

	public void set(final long i)
	{
		final int partitionId = (int) (i / 32);
		partitions[partitionId] = partitions[partitionId] & INVERSE_SELECT_INDEX[(int) (i % 32)];
	}

	public long countSetBits()
	{
		long setCount = 0;

		for (long i = 0, size = size(); i < size; i++)
		{
			if (isSet(i))
			{
				setCount++;
			}
		}

		return setCount;
	}

	public long size()
	{
		return partitions.length * 32l;
	}

	@Override
	public String toString()
	{
		final StringBuilder str = new StringBuilder();

		for (long i = 0, size = size(); i < size; i++)
		{
			if (isSet(i))
			{
				str.append("1");
			} else
			{
				str.append("0");
			}
		}

		return str.toString();
	}
}
