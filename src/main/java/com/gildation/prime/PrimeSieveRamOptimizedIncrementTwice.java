package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveRamOptimizedIncrementTwice implements PrimeSieve
{

	private final BitArray sieve;
	private final long effectiveSieveSize;

	public PrimeSieveRamOptimizedIncrementTwice(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes / 3);
		effectiveSieveSize = sieve.size() * 3 + 1;
		final long requiredComputeRange = (long) Math.sqrt(effectiveSieveSize) + 1;

		for (long i = 5; i < requiredComputeRange; i += 4)
		{
			if (sieve.isSet(safeIndexMap(i)))
			{
				unsetMultiples(i);
			}

			i += 2;

			if (sieve.isSet(safeIndexMap(i)))
			{
				unsetMultiples(i);
			}
		}
	}

	private void unsetMultiples(final long x)
	{
		for (long i = x * x, iIncr = x * 2; i < effectiveSieveSize; i += iIncr)
		{
			sieve.unset(safeIndexMap(i));
		}
	}

	@Override
	public boolean isPrime(final long x)
	{
		if (x == 2 || x == 3)
		{
			return true;
		} else
		{
			return sieve.isSet(safeIndexMap(x));
		}
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = effectiveSieveSize - 1;

		while (!sieve.isSet(safeIndexMap(i)))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return effectiveSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits() + 2;
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

	private long safeIndexMap(final long i)
	{
		final int offset = (int) (i % 6);
		final int isOddAnd = (int) -(i & 1);
		final int isOffsetNot3And = ~(-(offset & ((offset & 2) >> 1)));

		return (i / 3) & isOddAnd & isOffsetNot3And;
	}

}
