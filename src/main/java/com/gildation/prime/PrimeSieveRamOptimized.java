package com.gildation.prime;

import com.gildation.prime.models.BitArray;

public class PrimeSieveRamOptimized implements PrimeSieve
{

	private final BitArray sieve;
	private final long effectiveSieveSize;

	public PrimeSieveRamOptimized(final long numberOfPrimes)
	{
		sieve = new BitArray(numberOfPrimes / 3);
		effectiveSieveSize = sieve.size() * 3 + 1;
		final long requiredComputeRange = (long) Math.sqrt(effectiveSieveSize) + 1;

		for (long i = 6; i < requiredComputeRange; i += 6)
		{
			if (sieve.isSet(i / 3 - 1))
			{
				unsetMultiples(i - 1);
			}

			if (sieve.isSet(i / 3))
			{
				unsetMultiples(i + 1);
			}
		}
	}

	private void unsetMultiples(final long x)
	{
		for (long i = x * x; i < effectiveSieveSize; i += x)
		{
			sieve.unset(safeIndexMap(i));
		}
	}

	@Override
	public boolean isPrime(final long x)
	{
		if (x == 2 || x == 3)
		{
			return true;
		} else
		{
			return sieve.isSet(safeIndexMap(x));
		}
	}

	@Override
	public long findLargestPrimeFound()
	{
		long i = effectiveSieveSize - 1;

		while (!sieve.isSet(safeIndexMap(i)))
		{
			i--;
		}

		return i;
	}

	@Override
	public long effectiveSieveSize()
	{
		return effectiveSieveSize;
	}

	@Override
	public long countNumberOfPrimesFound()
	{
		return sieve.countSetBits() + 2;
	}

	@Override
	public long size()
	{
		return sieve.size();
	}

	@Override
	public String toString()
	{
		return sieve.toString();
	}

	private long safeIndexMap(final long i)
	{
		final int offset = (int) (i % 6);
		final int isOddAnd = (int) -(i & 1);
		final int isOffsetNot3And = ~(-(offset & ((offset & 2) >> 1)));

		return (i / 3) & isOddAnd & isOffsetNot3And;
	}

}
