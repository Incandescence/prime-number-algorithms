package com.gildation.prime.sieve.variations;

import java.util.PriorityQueue;

import com.gildation.prime.models.FactorPrimePair;

public class PrimeSieveLimitlessPriorityQueue
{
	public static void main(final String[] args)
	{
		long start, end;
		start = System.nanoTime();
		final PriorityQueue<FactorPrimePair> composites = new PriorityQueue<FactorPrimePair>();

		long i = 2;
		long j = 0;

		composites.add(new FactorPrimePair(i * i, i));
		i++;

		while (i < 100000)
		{

			if (composites.peek().getFactor().equals(i))
			{
				do
				{
					final FactorPrimePair entry = composites.poll();

					entry.incrementFactor();

					composites.add(entry);

				} while (composites.peek().getFactor().equals(i));
			} else
			{
				composites.add(new FactorPrimePair(i * i, i));
				j = i;
			}

			i++;
		}

		end = System.nanoTime();

		long k = 0;

		while (!composites.isEmpty())
		{
			final FactorPrimePair entry = composites.poll();

			if (entry.getPrime() > k)
			{
				k = entry.getPrime();
			}
		}

		System.out.println(i);
		System.out.println("Largest prime: " + j);
		System.out.println("Largest prime: " + k);
		System.out.println("Time taken: " + (end - start) / Math.pow(10, 9));
	}
}
