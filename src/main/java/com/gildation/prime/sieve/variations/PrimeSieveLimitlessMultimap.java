package com.gildation.prime.sieve.variations;

import java.util.Collection;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class PrimeSieveLimitlessMultimap
{
	public static void main(final String[] args)
	{
		long start, end;
		start = System.nanoTime();
		final Multimap<Long, Long> composites = ArrayListMultimap.create();

		long i = 2;
		long j = 0;

		while (i < 1000000)
		{
			final Collection<Long> factors = composites.get(i);
			if (factors.size() == 0)
			{
				composites.put(i * i, i);
				j = i;
			} else
			{
				for (final Long factor : factors)
				{
					composites.put(i + factor, factor);
				}

				composites.removeAll(i);
			}
			i++;
		}
		end = System.nanoTime();

		long k = i * i - 1;

		while (composites.get(k).size() == 0)
		{
			k--;
		}

		System.out.println(i);
		System.out.println("Largest prime: " + j);
		System.out.println("Largest prime: " + k);
		System.out.println("Time taken: " + (end - start) / Math.pow(10, 9));
	}
}
