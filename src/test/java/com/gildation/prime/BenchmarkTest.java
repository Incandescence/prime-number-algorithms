package com.gildation.prime;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;

public class BenchmarkTest
{

	public static final long range = 1000000000;
	public static final long repetitions = 1;

	@BeforeClass
	@AfterClass
	public static void seperatorStatic()
	{
		System.out.println("---------------------------------------------------------------------------------");
	}

	@Test
	public void benchmarkPrimeSieves() throws NoSuchMethodException, SecurityException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		final Reflections reflections = new Reflections("com.gildation.prime");
		final Set<Class<? extends PrimeSieve>> implementations = reflections.getSubTypesOf(PrimeSieve.class);

		PrimeSieve sieve;
		long timeStart;
		long timeEnd;
		double timeTaken;
		long numberOfPrimes;

		for (final Class<? extends PrimeSieve> implementation : implementations)
		{
			final Constructor<? extends PrimeSieve> constructor = implementation.getConstructor(Long.TYPE);

			System.out.println("---------------------------------------------------------------------------------");
			System.out.println(implementation.getSimpleName());
			System.out.println("---------------------------------------------------------------------------------");

			sieve = null;
			timeStart = 0;
			timeEnd = 0;

			timeStart = System.nanoTime();
			for (int i = 0; i < repetitions; i++)
			{
				sieve = constructor.newInstance(range);
			}
			timeEnd = System.nanoTime();

			timeTaken = ((timeEnd - timeStart) / Math.pow(10, 9)) / repetitions;

			numberOfPrimes = sieve.countNumberOfPrimesFound();

			System.out.println("Number of primes found:" + numberOfPrimes);
			System.out.println("Time taken: " + timeTaken);
			System.out.println("Average time to identify a prime: " + timeTaken / numberOfPrimes);
			System.out.println("Largest Prime: " + sieve.findLargestPrimeFound());
			System.out.println("Largest Prime: " + sieve.isPrime(sieve.findLargestPrimeFound()));
			System.out.println("Requested sieve size: " + range);
			System.out.println("Effective sieve size: " + sieve.effectiveSieveSize());
			System.out.println("Actual sieve size: " + sieve.size());
			System.out.println("---------------------------------------------------------------------------------");
		}
	}

}
